import React from 'react';
import styled from 'styled-components';

const StyledDiv = styled.div`
    span{
        font-size: 50px;
    }
    div{
        font-size: 12px;
    }
`

const Balance = ({balance, price}) => {
    
    return (
        <StyledDiv>
            <h1>Hello Balance</h1>
            <span>{Number(balance).toPrecision(5)}</span> Ethers 
            <div> {Number(balance*price).toPrecision(5)}$</div>
        </StyledDiv>
    )
}

export default Balance;