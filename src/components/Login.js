import React, { useState, useEffect } from 'react';
import Logo from './Logo';
import { 
    StyledInput,
    SingIn,
    Background,
    Content
 } from './style/LoginStyle';
import Article from './Article';
import { article } from '../data/article';
import { Link } from 'react-router-dom';
import { ethers } from 'ethers';

const isHex = (hex) => /^(-)?[0-9a-f]*$/i.test(hex);
export const isHexStrict = (hex) => /^(-)?0x[0-9a-f]*$/i.test(hex);

const isPrivateKey = (hex) => (isHexStrict(hex) && hex.length === 66 ) || (isHex(hex) && hex.length === 64 );

const Login = () => {
    const [value,setValue] = useState(localStorage.getItem('ethereum-password'));

    const onChange = (e) => {
        localStorage.setItem('ethereum-password', e.target.value);
        setValue(e.target.value);
        console.log(localStorage.getItem('ethereum-password'));
        console.log(`is key? ${isPrivateKey(value)}`)
    }

    useEffect(() => {
        return () => setValue(localStorage.getItem('ethereum-password'))
    })

    const handleClick = () => {
        console.log(`is hex? ${isHex(value)}`)
        console.log(`is hex strict? ${isHexStrict(value)}`)
        console.log(`is key? ${isPrivateKey(value)}`)
    }

    return (
        <div>
        <Background>
            <Logo title='Ethereum lightwallet' />
            <SingIn onClick={handleClick}>
                {isPrivateKey(value) ? <Link to="/dashboard">Sing in</Link> : 'Sing In'}
            </SingIn>
            <StyledInput maxLength="66" value={value} onChange={onChange} placeholder="Enter your ethereum password"/>
        </Background>
            <Content>
                    <Article article={article.article1}/>
                    <Article article={article.article2}/>
            </Content>
        </div>
    )
}

export default Login;