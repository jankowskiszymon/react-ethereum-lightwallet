import React from 'react';
import styled from 'styled-components';

const StyledDiv = styled.div`
    top:50%;
    left:50%;
    transform: translate(-50%,50%);
    position: absolute;
    font-size: 50px;

`

const Loading = () => (
    <StyledDiv>Loading...</StyledDiv>
)

export default Loading;