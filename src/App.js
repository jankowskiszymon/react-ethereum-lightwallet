import React, {useEffect} from 'react';
import Login from './components/Login';
import Footer from './components/Footer';
import { createGlobalStyle} from 'styled-components';
import Dashboard from './components/Dashboard';
import Logout from './components/Logout';
import { BrowserRouter as Router, Route, HashRouter } from "react-router-dom";
import History from './components/historyEth/History';


const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
  }
  ::-webkit-scrollbar{
    width: 0px;
  }
`

const App = () => {
  const data = {
    author: 'Szymon Jankowski',
    eth: '0x9ae2D92B75e99cc10bc2e34e2ec7159A3Ef43Ea6',
    path: {
      main: '/',
      dashboard: '/dashboard',
      logout: '/logout',
      history: '/history'
    }
  }

  useEffect(()=> localStorage.setItem('ethereum-password','8cd73c1ba1cc33d91906339f07f2e2431a4f5b66af937b12f651591ada436d9b'))
  
  return (
    <HashRouter>
      <div>
        <GlobalStyle />
        <Route exact path={data.path.main} component={Login}/>
        <Route exact path={data.path.dashboard} component={Dashboard}/>
        <Route exact path={data.path.logout} component={Logout}/>
        <Route exact path={data.path.history} component={History}/>
        <Footer author={data.author} eth={data.eth}/>
      </div>
    </HashRouter>
  )
}

//<GlobalStyle />
//<Route exact path={data.path.dashboard} component={Dashboard}/>
//<Route exact path={data.path.logout} component={Logout}/>

export default App;
