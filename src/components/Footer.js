import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

const StyledFooter = styled.div`
    position: fixed;
    bottom: 0;
    height: 30px;
    left: 0;
    text-align: center;
    width: 100vw;
    color: ${props => props.white ? 'white' : 'black'};
    background: ${props => props.white? 0 : 'white'};

    @media screen and (max-width: 1200px) {
         display: block;
         position: relative;
         width: 80vw;
         text-align: center;
         margin: 0 auto;
         i {
             display: none;
         }
    }
    `

const Footer = ({author, eth }) => {
    const [y,setY] = useState(window.pageYOffset);

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => (window.removeEventListener('scroll', handleScroll));
    });

    const handleScroll = () => setY(window.pageYOffset);
    return (
        <StyledFooter white={(y < 50)} onClick={() => console.log(`Global private key: ${localStorage.getItem('ethereum-password')}`)}>
            Made by {author}. <i>Donate ETH: {eth} </i>
        </StyledFooter>
    )

}

export default Footer;