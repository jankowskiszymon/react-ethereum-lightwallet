import styled from 'styled-components';

const Content = styled.div`
    margin-bottom: 30px;
    color: rgb(51,51,51);
    width: 100vw;
`

export default Content;