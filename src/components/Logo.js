import React from 'react';
import styled from 'styled-components';

const StyledDiv = styled.div`
    color: white;
    top: 20%;
    left: 50%;
    font-size: 50px;
    transform: translate(-50%,50%);
    position: absolute;
    width: 100%;
    text-align: center;
    animation-name: logo;
    animation-duration: 5s;
    -webkit-animation-direction: alternate-reverse;
    animation-iteration-count: infinite;

    @keyframes logo {
        0% {color: white;}
        50% {color: black;}
        100% {color:#cc0052;}
    }

`

const Author = styled.div`
    font-size: 20px;
`

const Logo = ({title, author}) => (<StyledDiv>{title}</StyledDiv>)

export default Logo;