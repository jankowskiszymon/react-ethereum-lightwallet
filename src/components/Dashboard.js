import React, { useEffect, useState } from 'react';
import { ethers } from 'ethers';
import ChartsPage from './Chart';
import styled from 'styled-components';
import axios from 'axios';
import { article } from '../data/article';
import Article from './Article';
import Acount from './Acount';
import { isHexStrict } from './Login';
import Balance from './Balance';
import { Link } from 'react-router-dom';

const url = 'https://min-api.cryptocompare.com/data/v2/histoday?fsym=ETH&tsym=USD&limit=50&api_key=7bd1e3c99bf9c1561f3aa26b146697a0ebd5250e3cbc02483a92c7ffdc05b0ae';

/*
            1. Info about acount
            2. Ethereum price and balance
            3. Recent Transaction 
            4. Sing Transaction 
            5. Ethereum price Chart
            6. Mini blog with fun things
*/

const FlexBox = styled.div`
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
    word-wrap: break-word;
    color: rgb(51,51,51);
    
`;

const Element = styled.div`
    margin-top: 10px;
    min-height: 20px;
    width: 80vw; 
    height: auto;
    margin-top: 30px;
    margin: 0 auto;
    display: ${props => props.top ? "flex" : 'default'};
    flex-wrap: wrap;
    text-align: ${props => props.history ? 'center' : 'default'};
    
    h1{
        color: #cc0052;
    }

    a{
        text-decoration: none;
        color:#cc0052;
    }

    @media (max-width: 1200px) {
        width: ${props=> props.chart ? '100vw' : '80vw'}
    }
    h1{
        p{
            text-align:center;
        }
    }
`

const Loading = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    width: 100vw;
    height: 100vh;
    background: white;
    p{
        position: absolute;
        top: 30%;
        left: 45%; 
        transform: translate(-50%,50%);
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #cc0052;
        width: 120px;
        height: 120px;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
    }
    @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
`

const checkKey = (hex) => isHexStrict(hex) ? hex : `0x${hex}`;

const Dashboard = ({}) => {
    const [ chartData, setChartData ] = useState('');
    const [ chartTimestamp, setChartTimestamp ] = useState('');
    const [ walletData, setWalletData ] = useState({address: ''})
    const [ history, setHistory] = useState('0');
    const [ balance, setBalance ] = useState('0');

    useEffect(() => {
        const privateKey = localStorage.getItem('ethereum-password');
        fetchData();
        getAddress(privateKey, (address) => {
            getHistory(address)
            getBalanceOfAddress('0xf91967b8F084de85C6785e4eA6B3540A7B3AD64D')
            localStorage.setItem('address',address)
        });
        
    }, [])

    const fetchData = async () => {
        const result = await axios(url);
        const setPrice = (ar) => {
            let price = [];
            for (let i = 0; i < ar.length; i++){
                price[i] = ar[i].close;
            }
            return price;
          };
        const setTimestamp = (ar) => {
            let price = [];
            for (let i = 0; i < ar.length; i++){
                price[i] = ar[i].time;
            }
            return price;
          };
        setChartData(setPrice(result.data.Data.Data))
        setChartTimestamp(setTimestamp(result.data.Data.Data))

    }

    const getAddress = async (privateKey, callback) => {
        const buforKey = checkKey(privateKey);
        let wallet = await new ethers.Wallet(buforKey);
        setWalletData({
            address: wallet.address
        })
        localStorage.setItem('address', wallet.address);
        await callback(wallet.address)
    }

    const getHistory = async (address) => {
        let provider = await new ethers.providers.EtherscanProvider();
        await provider.getHistory(address)
                .then(hist => setHistory({
                    history: hist
                }))
                .catch(err => console.log(err))
    }

    const getBalanceOfAddress = (address) => {
        let provider = new ethers.providers.EtherscanProvider();
        provider.getBalance(address)
                .then(balance => setBalance(parseInt(balance._hex,16)/1000000000000000000))
                .catch(err => console.log(err))
    }

    return (
        <FlexBox>
            <Element top>
                {walletData.address
                ?
                 <Acount 
                    address={walletData.address}
                    history={history.history}/> 
                : 
                'loading'}

                {balance ? <Balance balance={balance} price={182.32}/> : "Loading..."}
            </Element>
            <Element history>
                <h1>
                    <Link to="/history">History</Link>
                </h1>
            </Element>
            <Element chart>
                <h1><p>Ethereum price chart</p></h1>
                {chartData ? <ChartsPage priceData={chartData} time={chartTimestamp}/> : <Loading><p></p></Loading>}
            </Element>
            <Element>
                <Article article={article.article3}/>
            </Element>
            <Element>
                <Article article={article.article4}/>
            </Element>


        </FlexBox>
    )
}

export default Dashboard;