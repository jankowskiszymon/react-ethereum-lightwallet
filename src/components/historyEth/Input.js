import styled from 'styled-components';

export const InputButton = styled.input`
    height: 33px;
    width: 100px;
    background: #cc0052;
    margin-left: -30px;
    border: none;
    color: white;
    border-radius: 20px;
    border: 1px solid white;
    box-sizing: border-box;
    outline: none;
    z-index: 100;
    
    :hover{
        background: #00a3cc;
        cursor: pointer;
    }
`

const Input = styled.input`
    margin-top: 20px;
    height: 31px;
    width: 350px;
    color: lightgray;
    outline: none;
    border-radius: 10px;
    border: none;
    z-index: 10;
    font-size: 10px;
    box-sizing: border-box;

    ::placeholder{
        text-align: center;
        padding: 20px;
    }
    textarea{
        color: red;
    }

    @media (max-width: 1200px) {
        width: 70vw;
    }
`

export default Input;