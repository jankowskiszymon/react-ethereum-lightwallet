import styled from 'styled-components';

const Paragraph = styled.p`
    text-align: left;
    width: 80vw;
    word-wrap: break-word;
    margin: 0 auto;
    div{
        font-weight: bold;
    }
`

export default Paragraph;