This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Live version: [Live](http://highschoolblockchain.pl/#/) 
The live version is modified for the comfort of recruiters. Automatic password entry. Do not use this version for personal use.

###### Created by [Szymon Jankowski](https://www.facebook.com/petergrynn) for announcements

### About 

This is the simplest Ethereum wallet on Git. I wrote it myself for recruitment purposes.
You can model your wallet on this code. It will be much easier now. 

Warning!

The code is 100% written by me. I didn't use the rare guide written by an experienced programmer. So please, do not enter your realy genuine Ethereum key, and do not sign transactions on it! 

### Core components

- [ ] [Web3](https://github.com/ethereum/web3.js/) for connect to Ethereum
- [ ] [ethers.js](https://github.com/ethers-io/ethers.js/) for connect to Ethereum, too :)
- [ ] [Jest](https://jestjs.io/) for testing
- [ ] [styled-components](https://www.styled-components.com/) for design of dApp
- [ ] [Chars.js](https://www.chartjs.org/) for chart with live ethereum price data of last 50 days

### Api providers 

- [ ] [Cryptocompare](https://www.cryptocompare.com/) for live Ethereum price index

### Features

- [x] Login to Ethereum network
- [x] Eth auto balance to USD
- [x] Keep Ethereum key in LocalStorage
- [x] Mobile first.
- [x] Mini blog module. (components/article.js and data/article.js)
- [x] Chart with Ethereum price of last 50 days. Live data.
- [ ] Clear LocalStorage, simply logout
- [ ] Sign Transactions
- [ ] ERC20 support

### Development

3 majors components, each components is fully responsive! Try on mobile!
- Login - Simply for pass key
- Dashboard - Key from login is in LocalStorage. Dashboard component connect to Ethereum and pass data to other components.
- HistoryEth - You can choose the Ethereum address to view the transaction history


### How to use it

Programm your own cryptocurrency wallet on this code

```javascript
//Login.js
const isHex = (hex) => /^(-)?[0-9a-f]*$/i.test(hex); 
const isHexStrict = (hex) => /^(-)?0x[0-9a-f]*$/i.test(hex);

const isPrivateKey = (hex) => (isHexStrict(hex) && hex.length === 66 ) || (isHex(hex) && hex.length === 64 );
const checkKey = (hex) => isHexStrict(hex) ? hex : `0x${hex}`; //check ethereum password here 

//Dashboard.js
//
//Conect to Ethereum

useEffect(() => {
        const privateKey = localStorage.getItem('ethereum-password');
        //..

        //Callback function
        getAddress(privateKey, (address) => {
            getHistory(address)
            getBalanceOfAddress(address)
            localStorage.setItem('address',address)
        });
        
    }, []) //This line is important! It prevents loop.


//I make a function that gives us Ethereum Address from password. Thanks to Ethers.js. 
//The address is moved to callback function
const getAddress = async (privateKey, callback) => {
        const buforKey = checkKey(privateKey);
        let wallet = await new ethers.Wallet(buforKey);
        setWalletData({
            address: wallet.address
        })
        localStorage.setItem('address', wallet.address);
        await callback(wallet.address)
    }

//here we get history of Ethereum from EtherscanProvider
    const getHistory = async (address) => {
        let provider = await new ethers.providers.EtherscanProvider();
        await provider.getHistory(address)
                .then(hist => setHistory({
                    history: hist
                }))
                .catch(err => console.log(err))
    }

//simple get balance of address. We gives address as argument and it return converted to Ethers value.
    const getBalanceOfAddress = (address) => {
        let provider = new ethers.providers.EtherscanProvider();
        provider.getBalance(address)
                .then(balance => setBalance(parseInt(balance._hex,16)/1000000000000000000))//balance is returned ass hex string so I had to parseInt it to decimal and devide to get Ethers value.
                .catch(err => console.log(err))
    }


//get live price from cryptoCompare

const fetchData = async () => {
        const result = await axios(url);
        const setPrice = (ar) => {
            let price = [];
            for (let i = 0; i < ar.length; i++){
                price[i] = ar[i].close;
            }
            return price;
          };
        const setTimestamp = (ar) => {
            let price = [];
            for (let i = 0; i < ar.length; i++){
                price[i] = ar[i].time;
            }
            return price;
          };
        setChartData(setPrice(result.data.Data.Data))
        setChartTimestamp(setTimestamp(result.data.Data.Data))

    }

```


![Login](https://gitlab.com/petergrynn/react-ethereum-lightwallet/raw/master/src/readme/login.png)



![Responsive](https://gitlab.com/petergrynn/react-ethereum-lightwallet/raw/master/src/readme/responsive.jpg)

### Lincence

Completely MIT Licensed. Including ALL dependencies.