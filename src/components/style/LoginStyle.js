import styled from 'styled-components';
import Forest from '../../img/forest.jpg';

export const StyledInput = styled.input`
    height: 40px;
    width: 40vw;
    border: none;
    border-bottom: 3px solid white;
    background: none;
    color: white;
    outline: none;
    position: absolute;
    top: 75%;
    left: 50%;
    transform: translate(-50%,50%);
    
    text-align: center;
    ::placeholder{
        text-align: center;
        color: white;
    }
    :focus::placeholder{
        content: 0;
    }

    @media (max-width: 1000px){
        width: 60vw;
    }
`
export const SingIn = styled.button`
    color: white;
    top: 60%;
    left: 50%;
    transform: translate(-50%,50%);
    position: absolute;
    font-size: 40px;
    background: none;
    border: 0;
    outline: none;

    :hover{
        color: grey;
        cursor: pointer;
        
    }
    a {
        text-decoration: none;
        color: white;
    }
`

export const Background = styled.div`
    background-image: url(${Forest});
    position: relative;
    height: 100vh;
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
`

export const Content = styled.div`
    width: 40vw;
    background-color: white;
    margin-right: auto;
    margin-left: auto;
    font-size: 20px;
    box-sizing: border-box;
    
    @media (max-width: 1000px) {
        width: 80vw;
    } 
`   