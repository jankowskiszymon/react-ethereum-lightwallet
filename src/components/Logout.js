import React from 'react';

const Logout = () => (<div>{localStorage.getItem('ethereum-password')}</div>);

export default Logout;