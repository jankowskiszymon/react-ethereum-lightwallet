import React, { useEffect, useState } from "react";
import { Line } from "react-chartjs-2";
import styled from 'styled-components';

const StyledDiv = styled.div`
`

const ChartsPage = ({priceData, time}) => {

  const timestampToDate = (timestamp) => {
    let date = new Date(timestamp * 1000).toISOString().slice(5, 10).replace('T', ' ');
    return date;
  }

  const setTime = (ar) => {
    let result = [];
    for (let i = 0; i < ar.length; i++) {
      result[i] = timestampToDate(ar[i])
    }
    return result;
  }

  const data = { 
    dataLine: {
      labels: setTime(time),
      datasets: [
          {
          label: "Ethereum price index",
          fill: true,
          lineTension: 0.3,
          backgroundColor: "white",
          borderColor: "#cc0052",
          borderCapStyle: "butt",
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: "round",
          pointBorderColor: "",
          pointBackgroundColor: "rgb(255, 255, 255)",
          pointBorderWidth: 4,
          pointHoverRadius: 2,
          pointHoverBackgroundColor: "rgb(0, 0, 0)",
          pointHoverBorderColor: "rgba(220, 220, 220,1)",
          pointHoverBorderWidth: 2,
          pointRadius: 0,
          pointHitRadius: 10,
          data: priceData
          }
      ]
      }
  }

    return (
      <StyledDiv>
        <Line data={data.dataLine} options={{ responsive: true }} />
      </StyledDiv>
    );
}

export default ChartsPage;