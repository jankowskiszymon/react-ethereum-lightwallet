import React from 'react';
import styled from 'styled-components';
import Paragraph from './Paragraph';
import Moment from 'react-moment';
import Loading from './Loading';

Moment.globalFormat = 'D/MMM/YYYY HH:MM';

const Div = styled.div`
    border-top: 1px solid lightgray;
    border-bottom: 1px solid lightgray;
    width: 100vw;
    font-size: 18px;
    justify-content: space-between;
    box-sizing: border-box;
    z-index: 10000;
`

function hex2Ether(hex){
    let ethers = parseInt(hex);
    let ether = ethers / 1000000000000000000;
    return ether.toPrecision(2);
}

function HistoryComponent(props){
    return(
        <Div>
            {props.index}.
            <Paragraph title={props.history.hash}>
                <div>ThxHash:</div>
                {props.history.hash}
            </Paragraph>
            <Paragraph title={props.history.blockNumber}>
                <div>Block Number: </div>
                {props.history.blockNumber}
            </Paragraph>
            <Paragraph title={props.history.timestamp}>
                <div>Date: </div>
                <Moment unix>{props.history.timestamp}</Moment>
            </Paragraph>
            <Paragraph title={props.history.from}>
                <div>From:</div>
            {props.history.from}</Paragraph>
            <Paragraph title={props.history.to}>
                <div>To:</div>
            {props.history.to}
            </Paragraph>
            <Paragraph title={props.history.value._hex}>
                <div>Value: </div>
            {hex2Ether(props.history.value._hex)} Ethers
            </Paragraph>
        </Div>
    )
}

const HistoryEth = (props) => {
    const items = [];

    const history = props.history;
    console.log(props.history)
    
    for (let i = 0; i < history.length; i++) {
        items.push(<HistoryComponent index={i+1}history={props.history[i]}/>)
      }
    
    if(!props.history){
        return <Loading />
    }
    
    return(
        <div> 
            {items}
        </div>
    )
}

export default HistoryEth;