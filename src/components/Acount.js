import React, { useState, useEffect } from 'react';
import { ethers, providers } from 'ethers';
import styled from 'styled-components';

const StyledDiv = styled.div`
    text-align: left;
    font-weight: bold;
    width: 50%;
`
export const Info = styled.div`
    span{
        font-weight: normal;
    }

`

const Acount = ({address, history}) => {
    const [show, setShow] = useState(false);

    return (
        <StyledDiv>
            <h1>Hello Ethereum</h1>
            <Info>Address: </Info>
            <Info> 
                <span>{address}</span>
            </Info>
            <Info>
                    Transactions: 
            </Info>
            <Info>
                <span>
                    {history ? history.length : 0}
                </span>
            </Info>
            <Info>
                private key: 
            </Info>
            <Info onClick={() => setShow(!show)}>
                <span>
                    {show 
                    ? 
                    `0x${localStorage.getItem('ethereum-password')}` 
                    : 
                    'click to show private key'}
                </span>
            </Info>
        </StyledDiv>
    );
}

export default Acount;