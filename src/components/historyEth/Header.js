import styled from 'styled-components';

const Header = styled.div`
    height:150px;
    width: 100vw;
    top:0;
    right: 0;
    background: #cc0052;
    text-align: center;
    position: relative;
`

export default Header;