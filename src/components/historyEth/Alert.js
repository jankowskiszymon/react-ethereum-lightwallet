import React from 'react';
import styled from 'styled-components';

const Div = styled.div`
    top: 75px;
    z-index: 100;
    color: white;
    text-align: center;
    width: 100vw;
    word-wrap: break-word;

`

const Alert = ({address,transaction, balance}) => (
    <Div>Address: {address} Transaction: {transaction} Balance of address: {balance}</Div>
)

export default Alert;