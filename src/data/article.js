export const article = {
    article1: {
        id: '1',
        title: 'Hello Ethereum',
        info: '10-10-2019 Szymon Jankowski | 2 min of reading',
        content1: 'This application was created using the technology: React.js for architecture of app, Ethers.js and Web3.js for Ethereum Backend, CSS3 for style and Jest for testing. When I was writing the application, I designed applications using mobile first methodology. Right now, this is the simplest Ethereum Wallet on Github:',
        content2: 'On version 1.0 you can login to your Ethereum public key using prywate key and send/recive Ether. I could not find free and unlimited API for Ethereum price, so the price of Ethereum in USD is calculated from the formula: ETH.USD = BITCOIN.USD * 0.0225. You can also check your or any other Ethereum address history of transaction.',
        link: {
            href: 'https://gitlab.com/petergrynn/react-ethereum-lightwallet', title: 'LINK DO GITA'
        }
    },
    article2: {
        id: '2',
        title: 'About author',
        info: '11-10-2019 Szymon Jankowski | 1 min of reading',
        content1: `Hi! My name is Szymon and I am a creator of this app. I'm very interested in Blockchain technology and economics. I love programming and developing
        `,
        content2: 'I am looking for a job now. If you want me on board, write to me.',
        link: {
            href: '', title: ''
        }
    },
    article3: {
        id: '3',
        title: 'Enjoy using the Wallet',
        info: '11-10-2019 Szymon Jankowski | 2 min of reading',
        content1: 'Hi! You have just logged in to your Ethereum Acount, or you are simply on random acount. ',
        content2: '',
        link: {
            href: '', title: ''
        }
    },
    article4: {
        id: '4',
        title: 'Blog',
        info: '10-14-2019 Szymon Jankowski | 2 min of reading',
        content1: 'This application was created using the technology: React.js for architecture of app, Ethers.js and Web3.js for Ethereum Backend, CSS3 for style and Jest for testing. When I was writing the application, I designed applications using mobile first methodology. Right now, this is the simplest Ethereum Wallet on Github:',
        content2: 'On version 1.0 you can login to your Ethereum public key using prywate key and send/recive Ether. You can also check your or any other Ethereum address history of transaction.',
        link: {
            href: 'https://gitlab.com/petergrynn/react-ethereum-lightwallet', title: 'GIT'
        }
    }
}