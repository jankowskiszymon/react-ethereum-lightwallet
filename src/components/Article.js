import React from 'react';
import styled from 'styled-components';

const StyledDiv = styled.div`
    border-bottom: 3px dashed #cc0052;

    justify-content: left;
    h1 {
        color: #cc0052;
        padding: 0;
        margin-top: 7px;
        margin-bottom: 2px;
    }
    i {
        font-size: 13px;
        margin: 0;
    }
    div{
        margin-top: 5px;
        margin-bottom: 5px;
    }
    div:last-child{
        margin-bottom: 50px;
    }

    @media screen and (max-width: 1200px){
        div:last-child{
            margin-bottom: 10px;
        }
    }
`

const Article = ({article}) => {

    return (
        <StyledDiv>
            <h1>{article.title}</h1>
            <i>{article.info}</i>
            <div>{article.content1} <a href={article.link.href}>{article.link.title}</a></div>
            <div>{article.content2}</div>
        </StyledDiv>
    )
}

export default Article;